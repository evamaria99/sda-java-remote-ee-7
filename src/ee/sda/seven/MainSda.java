package ee.sda.seven;

// Class - is a template for your object
public class MainSda{

    // Function - it describes behaviour of your object

    // Functions should be always inside your class
    // Entry point

    // Function - it describes behaviour of your object

    // Functions should be always inside your class
    // Entry point

    // Function - it describes behaviour of your object

    // Functions should be always inside your class
    // Entry point

    // Example N - 1
/*    public static void main(String[] args) {
        // Start your code here

       int a = Integer.parseInt("123");
       double b = Double.parseDouble("23.6");

       int c = (int) b;
       long d = (long) b;

       // 34, 56,78 ..
        System.out.println(d);
    }*/

    // Example N - 2
/*    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("What's your age?");
        // It is user's input
        int age = scanner.nextInt(); // instruction

        {} - It is one statement
        if (age > 18){
            System.out.println("You can take drink");
        }
        else {
            System.out.println("You are not allowed");
        }

        // If I had an umbrella and was no raining, I would go outside
        // Else I would stay at home
    }*/

    // Example N - 3, how if else works

   /* public static void main(String[] args) {

        // Grading system
        Scanner scanner = new Scanner(System.in);

        System.out.println("What's your name?");

        String name = scanner.next();

        System.out.println("Student's name is " + name);

        System.out.println("What's your grade?");

        // 3.5, 5
        double grade = scanner.nextDouble();

        // Reverse of !(grade < 2) = grade > 2 && grade == 2
        if(grade < 2){
            // No there is no instruction
            System.out.println("Fail");
        }
        else if (grade >= 2.5 && grade < 3){
            System.out.println("D grade");
        }
        else if (grade >= 3 && grade < 4){
            System.out.println("C grade");
        }
        else if (grade == 5){
            System.out.println("A grade");
        }
        else {
            System.out.println("You entered invalid value");
        }


    }*/

    // Example N-4, Switch

   /* public static void main(String[] args) {

        int grade = 4;

        switch (grade){
            case 2:
                System.out.println("Fail");
                break;
            case 3:
                System.out.println("C grade");
                break;
            case 4:
                System.out.println("B grade");
                break;
            case 5:
                System.out.println("A grade");
                break;
            default:
                break;
        }
    }*/

    // Example N-5, Switch with string

   /* public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Is it possible to convert from int to string? (Y/N)");

        String firstAnswer = scanner.next();

        System.out.println("Can we use switch instead of If else statement? (Y/N)");

        String secondAnswer = scanner.next();

        // Camel case
        // YY, YN, NN, NY
        String combineTwoAnswers = firstAnswer + secondAnswer;

        switch (combineTwoAnswers){
            case "YY":
                System.out.println("Senior");
                break;
            case "YN":
                System.out.println("Middle");
                break;
            case "NN":
                System.out.println("Junior");
                break;
            case "NY":
                System.out.println("Middle to Senior");
                break;
            default:
                System.out.println("Invalid value, please enter correct values like Y or N");
                break;
        }
    }*/

    // Example N-6, Why do we need a loop?
 /*   public static void main(String[] args) {
        // Print numbers from 1 to 10 to the screen

*//*        int one = 1;
        int two = 2;
        // ...

        // 1
        // 2
        // 3
        // 4
        // ...
        System.out.println(one);
        System.out.println(two);
        System.out.println(3);
        System.out.println(4);
        System.out.println(5);
        System.out.println(6);
        System.out.println(7);
        System.out.println(8);
        System.out.println(9);
        System.out.println(10);*//*
        // ...

        // While I was walking outside, it was raining and did not have an umbrella

        int i = 1;
        while (i <= 10){
            System.out.println(i);

            // Increment it by one
            i = i + 2;
            // Now i becomes 3
            //
        }
    }*/

 /*   // Example N-7, For loop
    public static void main(String[] args) {
        // For loop has three blocks
        // 1. We need to put initial value
        // 2. When loop should be stopped
        // 3. Which value should be incremented
        for(int i=1; i<=10; i++){
            System.out.println(i);
        }

        // Additional rules:
        // i = i - 4;
        // i-=4;

        // i = i + 1;
        // i++;
        // i = i - 1
        // i--;
    }*/

  /*  // Example N-8, For loop decrement
    public static void main(String[] args) {
        // Print numbers from 10 up to 1

        for (int i = 10; i>=1; i--){
            System.out.println(i);
        }
    }*/

    // Example N - 9, Understanding functions
   /* public static void main(String[] args) {

        boolean weekday = false;
        boolean vacation = true;

        if(weekday == true && vacation == false){
            System.out.println("We are not sleeping");
        } else {
            System.out.println("We are sleeping");
        }


    }
*/
    // Arguments or parameters
    // void - means, function returns nothing
    // They are all functions
    /*public static boolean sleepIn(boolean weekday, boolean vacation) {

        if(weekday == true && vacation == false){
            return false;
        } else {
            return true;
        }
    }*/

    // Example N - 10

    public static void main(String[] args) {

        /**
         * * * * * * *
         * * * * * * *
         * * * * * * *
         * * * * * * *
         * * * * * * *
         * * * * * * */

/*        System.out.println("* * * * * * *");
        System.out.println("* * * * * *");
        System.out.println("* * * * *");
        System.out.println("* * * * ");
        System.out.println("* * * ");
        System.out.println("* *");
        System.out.println("*");*/

        int numOfStars = 7;

        // Outer loop
        // Rows
        for (int i = 1; i <= numOfStars; i++) {

            // Inner loop
            // Columns
            for (int j = numOfStars; j >= i; j--) {
                System.out.print("*");
            }

            System.out.println();
        }

        // Task 1. Try to draw a square with rows and columns

        /**
         *
         * *
         * * *
         * * * *
         * * * * *
         * * * * * *
         */
    }
}








































































































































































































































































































































































































































































































































































































































































































































































































































































































