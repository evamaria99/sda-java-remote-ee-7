package ee.sda.seven.objectandclasses.addressbook;

public class AddressBookDemo {

    public static void main(String[] args) {

        Person person1 = new Person("John", "Smith", 30);

        Address address1 = new Address("11912");

        address1.setCity("Tallinn");
        address1.setCountry("Estonia");
        address1.setStreet("Roosikrantsi");
        address1.setHouseNumber(10);

        person1.setAddress(address1);

        System.out.println(person1.getFirstName() + " lives " + person1.getAddress().getCity());

        // TODO 1: Please choose two objects, created classes for them
        // and make a relationship
    }
}
