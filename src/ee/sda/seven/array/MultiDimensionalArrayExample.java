package ee.sda.seven.array;

import java.util.Scanner;

public class MultiDimensionalArrayExample {

    public static void main(String[] args) {

        int[][] arr = new int[2][2];
        // [][]
        // [][]

        arr[0][0] = 4;
        // [4][]
        // [6][]
        arr[1][0] = 6;

        MultiDimensionalArrayExample example = new MultiDimensionalArrayExample();

        example.printTwoDimensionalArray(arr);

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter row length of an array:");
        int row = scanner.nextInt();
        System.out.println("Enter column length of an array:");
        int column = scanner.nextInt();

        int[][] userArr = new int[row][column];

        System.out.println("Enter this amount of " + row * column +" elements to store in array");

        // User enters all values
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                userArr[i][j] = scanner.nextInt();
            }
        }

        System.out.println("Elements in your array are: ");

        example.printTwoDimensionalArray(userArr);
    }

    public void printTwoDimensionalArray(int[][] userArr){
        for (int i = 0; i < userArr.length; i++) {
            for (int j = 0; j < userArr[i].length; j++) {
                System.out.println("arr[" + i + "][" + j + "] = "
                        + userArr[i][j]);
            }
        }
    }
}
