package ee.sda.seven.array;

public class FindSecondSmallest {

    // Write a java program to find second smallest
    // element from an array

    // Input:
    // 1, 5, 2, 4, 7, 6, 3, 4
    // Output:
    // 2

    public static void main(String[] args) {
        int[] arr = {1, 5, 2, 4, 7, 6, 3, 4};

        int firstMin = Integer.MAX_VALUE;
        int secondMin = Integer.MAX_VALUE;

        // Loop enhancement
        // Shorter version of loop
/*        for (int i : arr) {
            System.out.println(i);
        }*/

        for (int i : arr) {
            // First condition
            // 1 < 9999999999...
            // 5 < 1
            // 2 < 1
            if (i < firstMin){
                firstMin = i;
                // firstMin = 1;
            }
            // Last condition
            // 1 < 9999999999...
            // 5 < 1
            else if (i < secondMin){
                secondMin = i;
                // secondMin = 1;
            }
            // ends
        }

        System.out.println("Second lowest element is :" +secondMin);
    }
}
