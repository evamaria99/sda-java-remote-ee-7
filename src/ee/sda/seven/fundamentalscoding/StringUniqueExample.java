package ee.sda.seven.fundamentalscoding;

import java.util.Scanner;

/**
 * Check if given string has all unique chars
 *
 * Apple - has not all unique chars
 * World - has all unique chars
 */
public class StringUniqueExample {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Write a word:");

        String word = scanner.next();

        System.out.println("Given string is unique: " +IsUnique(word));
    }

    public static boolean IsUnique(String word){

        for (int i = 0; i < word.length(); i++) {

            char letter = word.charAt(i);

            if(word.indexOf(letter) != word.lastIndexOf(letter)){
                return false;
            }
        }

        return true;
    }
}
