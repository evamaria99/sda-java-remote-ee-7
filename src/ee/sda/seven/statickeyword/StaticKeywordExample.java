package ee.sda.seven.statickeyword;

public class StaticKeywordExample {

    public static void main(String[] args) {

        // Create some students in Computer science faculty
        Student student1 = new Student();
        Student student2 = new Student();
        Student student3 = new Student();
        Student student4 = new Student();
        //... and 16 more students
        // They are studying in one faculty

        student1.setAge(20);
        student1.setFirstName("John");
        student1.setLastName("Smith");

        student2.setAge(23);
        student2.setFirstName("Anna");
        student2.setLastName("Smith");

        System.out.println(Student.faculty);

        Student.study();
    }
}
