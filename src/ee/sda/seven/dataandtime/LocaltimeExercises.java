package ee.sda.seven.dataandtime;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;

public class LocaltimeExercises {

    public static void main(String[] args) {

        // Write a small program to get the months remaining in the year

        LocalDate today = LocalDate.now();
        LocalDate lastDayOfYear = today.with(TemporalAdjusters.lastDayOfYear());

        Period period = today.until(lastDayOfYear);

        System.out.println("Months remaining in the year 2021: " +period.getMonths());
    }
}
